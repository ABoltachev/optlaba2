import math
import time
import sys

import cplex

from DIMACS_Graph import DIMACS_Graph


def timer(f):
    def wrap(*args):
        time1 = time.time()
        ret = f(*args)
        time2 = time.time()
        return ret, '{0:.3f} ms'.format((time2 - time1) * 1000.0)
    return wrap


class Solver:
    def __init__(self, graph: DIMACS_Graph):
        self.__graph = graph
        self.__branch_num = 0
        self.__cur_sol = None
        self.__upper_bound = None
        self.__eps = 0.000001
        self.__lp_problem = self.__create_lp_problem()

    def __create_lp_problem(self):
        obj = [1.0] * self.__graph.count_vert
        upper_bounds = [1.0] * self.__graph.count_vert
        types = 'C' * self.__graph.count_vert

        columns_names = [f'x{x}' for x in self.__graph.nodes]
        right_hand_side = [1.0] * (len(self.__graph.init_states) + len(self.__graph.not_connected_edges))
        name_iter = iter(range(len(self.__graph.init_states) + len(self.__graph.not_connected_edges) ** 2))
        constraint_names = [f'c{next(name_iter)}' for x in range(
            (len(self.__graph.init_states) + len(self.__graph.not_connected_edges)))]
        constraint_senses = ['L'] * (len(self.__graph.init_states) + len(self.__graph.not_connected_edges))

        problem = cplex.Cplex()
        problem.set_log_stream(None)
        problem.set_results_stream(None)
        problem.set_warning_stream(None)
        problem.set_error_stream(None)
        problem.objective.set_sense(problem.objective.sense.maximize)
        problem.variables.add(obj=obj, ub=upper_bounds,
                              names=columns_names, types=types)

        constraints = []
        for ind_set in self.__graph.init_states:
            constraints.append([[f'x{x}'
                                 for x in ind_set], [1.0] * len(ind_set)])
        for xi, xj in self.__graph.not_connected_edges:
            constraints.append(
                [[f'x{xi}', f'x{xj}'], [1.0, 1.0]])

        problem.linear_constraints.add(lin_expr=constraints,
                                       senses=constraint_senses,
                                       rhs=right_hand_side,
                                       names=constraint_names)
        return problem

    @staticmethod
    def __get_branching_variable(solution: list):
        return max(list(filter(lambda x: not x[1].is_integer(), enumerate(solution))),
                   key=lambda x: x[1], default=(None, None))[0]

    def __branching(self):
        def add_constraint(bv: float, rhs: float, cur_branch: int):
            self.__lp_problem.linear_constraints.add(lin_expr=[[[bv], [1.0]]],
                                                     senses=['E'],
                                                     rhs=[rhs],
                                                     names=[f'branch_{cur_branch}'])

        try:
            self.__lp_problem.solve()
            opt_point = self.__lp_problem.solution.get_values()
            solution = self.__lp_problem.solution.get_objective_value()
        except cplex.exceptions.CplexSolverError as ex:
            print(ex)
            return 0

        if self.__cur_sol is not None and (solution - self.__cur_sol) <= self.__eps:
            return 0
        if self.__upper_bound is not None and (solution - self.__upper_bound) > self.__eps and self.__cur_sol is not None:
            return 0
        bvar = self.__get_branching_variable(opt_point)
        if bvar is None:
            self.__cur_sol = len(
                list(filter(lambda x: math.fabs(x - 1.0) <= self.__eps, opt_point)))
            print(f'Current sol: {self.__cur_sol}', )
            return self.__cur_sol, opt_point
        else:
            self.__branch_num += 1
            cur_branch = self.__branch_num
            cur_branch += 1

            add_constraint(bvar, 1.0, cur_branch)
            branch_1 = self.__branching()
            self.__lp_problem.linear_constraints.delete(f'branch_{cur_branch}')
            add_constraint(bvar, 0.0, cur_branch)
            branch_2 = self.__branching()
            self.__lp_problem.linear_constraints.delete(f'branch_{cur_branch}')
            return max([branch_1, branch_2], key=lambda x: x[0] if isinstance(x, (list, tuple)) else x)

    @timer
    def solve(self):
        self.__upper_bound = self.__graph.greedy_coloring_heuristic()
        return self.__branching()


def main():
    sys.setrecursionlimit(50000)
    filepath = '/home/aboltachev/Downloads/DIMACS_all_ascii/brock200_1.clq'
    graph = DIMACS_Graph.load_graph(filepath)
    solver = Solver(graph)
    solution, durations = solver.solve()

    print('Maximum clique size: ', solution[0])
    print('Nodes: ', list(index + 1 for index, value in enumerate(solution[1]) if value == 1.0))
    print('Durations: ', durations)


if __name__ == '__main__':
    main()
