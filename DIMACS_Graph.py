import networkx as nx


class DIMACS_Graph:
    def __init__(self):
        self.__graph = None
        self.__count_vert = 0
        self.__count_edg = 0
        self.__not_connected_edges = []
        self.__init_states = []

    @property
    def graph(self):
        return self.__graph

    @property
    def nodes(self):
        return self.__graph.nodes

    @property
    def count_vert(self):
        return self.__count_vert

    @property
    def count_edg(self):
        return self.__count_edg

    @property
    def not_connected_edges(self):
        if not self.__not_connected_edges and self.__graph:
            self.__not_connected_edges = nx.complement(self.__graph).to_directed().edges
        return self.__not_connected_edges

    @property
    def init_states(self):
        if not self.__init_states and self.__graph:
            self.__create_init_states()
        return self.__init_states

    def __create_init_states(self):
        strategies = [nx.coloring.strategy_largest_first,
                      nx.coloring.strategy_random_sequential,
                      nx.coloring.strategy_independent_set,
                      nx.coloring.strategy_connected_sequential_bfs,
                      nx.coloring.strategy_connected_sequential_dfs,
                      nx.coloring.strategy_saturation_largest_first]

        for strategy in strategies:
            d = nx.coloring.greedy_color(self.graph, strategy=strategy)
            for color in set(color for node, color in d.items()):
                self.__init_states.append(
                    [key for key, value in d.items() if value == color])

    def greedy_coloring_heuristic(self):
        color_num = iter(range(0, len(self.__graph)))
        color_map = {}
        nodes = [node[0] for node in sorted(nx.degree(self.__graph),
                                            key=lambda x: x[1], reverse=True)]
        color_map[nodes.pop(0)] = next(color_num)
        used_colors = {i for i in color_map.values()}
        while len(nodes) != 0:
            node = nodes.pop(0)
            neighbors_colors = {color_map[neighbor] for neighbor in self.__graph.neighbors(node)
                                if neighbor in color_map}
            if len(neighbors_colors) == len(used_colors):
                color = next(color_num)
                used_colors.add(color)
                color_map[node] = color
            else:
                color_map[node] = next(iter(used_colors - neighbors_colors))
        return len(used_colors)

    @classmethod
    def load_graph(cls, filepath: str):
        graph = cls()
        edges = []
        for line in open(filepath):
            type_line, *content = line.split()

            if type_line == 'p':
                graph.__count_vert, graph.__count_edg = (int(x) for x in content[1:])
            elif type_line == 'e':
                edge = [int(x) for x in content]
                edges.append(edge)

        graph.__graph = nx.Graph(edges)
        return graph
